#ifndef MIRROFINDER_H
#define MIRROFINDER_H

#include <QObject>
#include <QString>
#include <QList>
#include <QMap>
#include <QDebug>
#include <QTime>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QRegExp>
#include <QSysInfo>
#include <QSettings>
#include <QDir>

class MirroFinder : public QObject
{
    Q_OBJECT
public:
    explicit MirroFinder(QObject *parent = nullptr);

signals:

public slots:
    //void readFile(QString addressFile);
    void readLinksFirstItemOfListRpm();
    void readLinksFirstItemOfListFedora();
    void downloadItemsFedora(QUrl url);
    void downloadItemsRpmFusion(QUrl url);

    void downloadItemsFedoraFinished(QNetworkReply *reply);
    void downloadItemsRpmFusionFinished(QNetworkReply *reply);

    void downloadWebsitRpmFusion(QUrl url);
    void findLinksRpm(QNetworkReply *findLinks);

    //void downloadWebsitRpmFusion(QUrl url);
    //void downloadWebsitFedora(QUrl url);
    void downloadWebsitFedora(QUrl url);
    void findLinksFedora(QNetworkReply *findLinksFedora);

    //QString findLinksInTheList();

    void findInfoSystem();
    void replaceToIniFileForFedora();
    void replaceToIniFileForRpm();

    void writeToFileFedora(QString addressFile);
    void writeToFileRpmFusion(QString addressFile);


    void createDynamicLink(QString dynamic , QString whatis);
    void downloadDynamicLink(QString url , QString whatis);
    void createPatchForIni();
    void downloadFinishedDynamicLinkFedora(QNetworkReply *downloadFinishedDynamicLinkFedora);
    void downloadFinishedDynamicLinkRpmfusion(QNetworkReply *downloadFinishedDynamicLinkRpmFusion);

    void fixSharp(const QString &path);
    void start();

protected:
    void readLinksFirstItemOfListFedoraFinished();
    void readLinksFirstItemOfListRpmFuisionFinished();

    void printFirstLinkFedora();
    void printFirstLinkRpmFusion();

private:

    QNetworkRequest _request;
    QNetworkAccessManager * _manager;
    QList<QString>_queuFedora;
    QList<QString>_queuRpm;

    QMap<int,QString>_calculateTimeFedora;
    QMap<int,QString>_calculateTimeRpm;

    QUrl _URL;
    QTime _time;
    int _ntimeCurrent;
    QString _nmyLinks;
    QFile _openFile;

    QString _linksRpm;
    QString _linksFedora;

    QStringList _listLinksRpm;
    QStringList _listLinkFedora;
    QString _LinkDynamicFedora;
    QString _LinkDynamicRpm;

    QByteArray data;
    QString _finalBestLinkFedora;
    QString _finalBestLinkRpm;
    QSysInfo productVersion;
    QSysInfo currentCpuArchitecture;

    QString _releaseever;
    QString _baseArch;

    QString _aIniFileF; //Fedora
    QString _aIniFileFU; //fedora-update
    QString _aIniFileRF; //rpmfusion-free
    QString _aIniFileRFU; //rpmfusion-free-update
    QString _aIniFileRFUNonFree;//rpmfusion-nonfree
    QString _aIniFileRFUNonFreeUpdate;//rpmfusion-nonfree-update
    QString _dynamicLinkIniFile;

    QRegExp regexRpmFusion{"http(:?s|)://(\bftp-stud|ftp.tu|fr2|mir01|download1|www.mirrorservice|www.fedora|kartolo|muug|mirror|mirrors|repo|ucmirror|ftp|fedora|rpmfusion|)\\.\\w*(:?\\.|)(:?-|)(:?/|)\\w*(:?/|)(:?\\.|)\\w*(:?/|)(:?\\.|)(:?\\w*|)(:?/|)(:?\\.|)(:?\\w*|)(:?/|)(:?\\w*|)(:?\\.|)(:?/|)(:?\\w*|)(:?\\w*|)(\b/free/fedora|/RPMFUSION_free_Fedora|)"};
    //QRegExp regexFedora{"http(:?s|)://(:?\\w*|)(:?\\.|-)(:?\\w*)(:?\\.|-)(:?\\w*|)(:?/|)(:?\\w*)(:?\\.|/:?-|)(:?/|\\w*)(:?\\w*|/)(:?/|\\.:?-|)(:?\\w*|)(:?\\.|/:?-|)(:?/|-:?\\.|)(:?\\w*|-)(:?-|/)(:?\\w*)(:?/|)(:?-|)(:?\\w*|)(:?/|\\.:?-|)(:?-|\\w*:?\\.|)(:?\\w*|/)(:?/|)(:?\\w*|\\.)(:?/|)(:?\\.|)(:?\\w*|)(:?/|-:?-|)(:?\\w*|\\.)(:?-|/:?\\.|)(:?\\w*|/:?\\.|)(:?/|)(:?\\w*|\\.)(:?/|)(:?\\.|\\w*)(:?\\w*|/)(:?\\w*|/)(:?-|)(:?/|\\w*)(:?/|\\w*)(:?/|\\w*)(:?\\w*|/)(:?/|)(:?\\w*|)(:?/|)"};
    QRegExp regexFedora{"http(:?s|)://(\bfedora-mirror02|download-ib01|pubmirror1|pubmirror2|fr2|mirror2|mirror1|fedora-mirror|distrib-coffee|ftp-stud|www.nic|www.fedora|www.ftp|kambing.ui|www.mirrorservice|espejito|spout|sjc|repo|mirror|ftp|syd|mirrors|lon|hkg|kartolo|nrt|my|ams|ucmirror|sg|linus|dfw|ewr|iad|ord|fedora)(:?\\.|-)(:?\\w*)(:?/|)(:?\\w*|)(:?/|)(:?\\w*|)(:?\\.|)(:?-|)(:?/|)(:?\\w*|)(:?/|)(:?\\w*)(:?\\.|/:?-|)(:?/|\\w*)(:?\\w*|/)(:?/|\\.:?-|)(:?\\w*|)(:?\\.|/:?-|)(:?/|-:?\\.|)(:?\\w*|-)(:?-|/)(:?\\w*)(:?/|)(:?-|)(:?\\w*|)(:?/|\\.:?-|)(:?-|\\w*:?\\.|)(:?\\w*|/)(:?/|)(:?\\w*|\\.)(:?/|)(:?\\.|)(:?\\w*|)(:?/|-:?-|)(:?\\w*|\\.)(:?-|/:?\\.|)(:?\\w*|/:?\\.|)(:?/|)(:?\\w*|\\.)(:?/|)(:?\\.|\\w*)(:?\\w*|/)(:?\\w*|/)(:?-|)(:?/|\\w*)(:?/|\\w*)(:?/|\\w*)(:?\\w*|/)(:?/|)(:?\\w*|)(:?/|)"};

    QRegExp regexDynamicFedora{"astromenace(:?-|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?-|)(:?\\d|\\d\\d)\\.\\w*\\.x86_64.rpm"};
    QRegExp regexDynamicRpmFuison{"aegisub(:?-|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?-|)(:?\\d\\d|\\d\\d\\d)(:?\\.)\\w*\\.x86_64.rpm"};

};
#endif // MIRROFINDER_H

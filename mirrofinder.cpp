#include "mirrofinder.h"

#define ROOT_ETC_YUM_PATH "/etc/yum.repos.d/"
#define BYTE_DOWNLOAD_ 500000

MirroFinder::MirroFinder(QObject *parent) : QObject(parent)
{
}

void MirroFinder::start()
{
    findInfoSystem();
    createPatchForIni();

    QUrl adressFedora{"https://admin.fedoraproject.org/mirrormanager/mirrors/Fedora/30/x86_64"};
    downloadWebsitFedora(adressFedora);
}

void MirroFinder::downloadWebsitFedora(QUrl url)
{
    qDebug() << __FUNCTION__;

    _manager = new QNetworkAccessManager(this);
    connect(_manager , &QNetworkAccessManager::finished , this , &MirroFinder::findLinksFedora);

    _URL = url;
    _request.setUrl(_URL);
    _manager->get(_request);
}

void MirroFinder::findLinksFedora(QNetworkReply *findLinksFedora)
{
    qDebug() << __FUNCTION__;

    data=findLinksFedora->readAll();
    _linksFedora=data;

    unsigned long positionStart = _linksFedora.indexOf("<table>");
    unsigned long positionFinish = _linksFedora.indexOf("</table>");

    while ((positionStart = regexFedora.indexIn(_linksFedora, positionStart)) <= positionFinish)
    {
        _listLinkFedora << regexFedora.cap();
        if(positionStart <= positionFinish)
            positionStart += regexFedora.matchedLength();
    }

    QStringList filterList;
    filterList =  _listLinkFedora.filter("epel");

    for (int i =0 ; i < _listLinkFedora.count();i++)
    {
        for (int b = 0 ; b < filterList.count(); b++)
        {
            if(_listLinkFedora.at(i) == filterList.at(b))
            {
                _listLinkFedora.removeAt(i);
            }
        }
    }

    QStringList list = _listLinkFedora;
    list = list.toSet().toList();
    _listLinkFedora = list;

    qDebug() << _listLinkFedora.join("\n").toStdString().c_str();
    qDebug() << _listLinkFedora.count() << "Links found";

    _queuFedora = _listLinkFedora;

    readLinksFirstItemOfListFedora();


}

void MirroFinder::readLinksFirstItemOfListFedora()
{
    qDebug() << __FUNCTION__ << _queuFedora.count() << "Items remained";

    if(_queuFedora.isEmpty())
    {
        printFirstLinkFedora();
    }

    else
    {

        QString downloadFirstItem =  _queuFedora.takeFirst();
        createDynamicLink(downloadFirstItem , "fedora");
    }
}

void MirroFinder::readLinksFirstItemOfListFedoraFinished()
{
    qDebug() << __FUNCTION__ << _dynamicLinkIniFile << _LinkDynamicFedora;
    QString finishLinkForItems = _dynamicLinkIniFile + _LinkDynamicFedora;

    _time.start();
    downloadItemsFedora(finishLinkForItems);
}


void MirroFinder::downloadItemsFedora(QUrl url)
{
    qDebug() << __FUNCTION__ << url;

    _manager = new QNetworkAccessManager(this);
    connect(_manager , &QNetworkAccessManager::finished , this , &MirroFinder::downloadItemsFedoraFinished);
    _request.setRawHeader("Range" , "bytes=" + QByteArray::number(0) + "-" + QByteArray::number(BYTE_DOWNLOAD_));
    _URL = url;
    _request.setUrl(_URL);
    _manager->get(_request);
}

void MirroFinder::printFirstLinkFedora()
{
    qDebug() << __FUNCTION__;
    QString cutTheLinkFedora = _calculateTimeFedora.value(_ntimeCurrent);
    int pos1 =cutTheLinkFedora.lastIndexOf(QString("/releases"));
    _finalBestLinkFedora = cutTheLinkFedora.left(pos1);
    qDebug()  << _calculateTimeFedora.value(_ntimeCurrent);
    replaceToIniFileForFedora();
}


void MirroFinder::downloadItemsFedoraFinished(QNetworkReply * reply)
{
    qDebug() << __FUNCTION__;
    if(reply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    }

    _ntimeCurrent = _time.elapsed();
    QString finishLinkForItems  = _URL.toString();
    _calculateTimeFedora.insert(_ntimeCurrent , finishLinkForItems);
    qDebug() << __FUNCTION__ << _ntimeCurrent << finishLinkForItems;
    readLinksFirstItemOfListFedora();
}

void MirroFinder::findInfoSystem()
{

    _releaseever =  productVersion.productVersion();
    _baseArch = currentCpuArchitecture.currentCpuArchitecture();
}

void MirroFinder::createPatchForIni()
{
    QDir dir(ROOT_ETC_YUM_PATH "IniFiles");

    if (!dir.exists())
    {
        dir.mkpath(ROOT_ETC_YUM_PATH "IniFiles");
    }

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmRepo.ini" , QSettings::IniFormat);

    settings.beginGroup("fedora");
    settings.setValue("alghorithmFedoraMain" , "$link/releases/$releasever/Everything/$basearch/os/");
    settings.endGroup();

    settings.beginGroup("fedora-update.repo");
    settings.setValue("alghorithmFedoraUpdateMin" ,"$link/updates/$releasever/Everything/$basearch/");
    settings.endGroup();

    settings.beginGroup("rpmfusion-free.repo");
    settings.setValue("alghorithmRpmFusion" ,"$link/free/fedora/releases/$releasever/Everything/$basearch/os/");
    settings.endGroup();

    settings.beginGroup("rpmfusion-free-updates.repo");
    settings.setValue("alghorithmRpmfusionFreeUpdate" ,"$link/free/fedora/updates/$releasever/$basearch/");
    settings.endGroup();

    settings.beginGroup("rpmfusion-nonfree");
    settings.setValue("alghorithmRpmfusionNonFree" ,"$link/nonfree/fedora/releases/$releasever/Everything/$basearch/os/");

    settings.beginGroup("rpmfusion-nonfree-updates");
    settings.setValue("alghorithmRpmfusionNonFreeUpdate" ,"$link/nonfree/fedora/update/$releasever/$basearch/");

    QSettings settingsDynamicLink(ROOT_ETC_YUM_PATH "IniFiles/alghoritmDynamicLink.ini" , QSettings::IniFormat);
    settingsDynamicLink.beginGroup("dynamicLink");
    settingsDynamicLink.setValue("alghorithmDynamicLink" ,"$link/releases/$releasever/Everything/$basearch/os/Packages/a/");
    settingsDynamicLink.endGroup();
}

void MirroFinder::replaceToIniFileForFedora( )
{
    qDebug() << __FUNCTION__;

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmRepo.ini", QSettings::IniFormat);

    //Alghoritm files fedora
    settings.beginGroup("fedora");
    _aIniFileF = settings.value("alghorithmFedoraMain").toString();
    settings.endGroup();

    _aIniFileF.replace("$link" , _finalBestLinkFedora);
    _aIniFileF.replace("$basearch" , _baseArch);
    _aIniFileF.replace("$releasever" ,_releaseever);


    //Alghoritm files fedora-updates
    settings.beginGroup("fedora-update");
    _aIniFileFU = settings.value("alghorithmFedoraUpdateMin").toString();
    settings.endGroup();

    _aIniFileFU.replace("$link" , _finalBestLinkFedora);
    _aIniFileFU.replace("$basearch" , _baseArch);
    _aIniFileFU.replace("$releasever" ,_releaseever);

    writeToFileFedora(ROOT_ETC_YUM_PATH);
}

void MirroFinder::writeToFileFedora(QString addressFile)
{
    QDir directory(addressFile);

    QStringList repoFiles = directory.entryList(QStringList() << "*.repo" ,QDir::Files);

    for (int i = 0 ; i <= repoFiles.size(); i++)
    {
        if(repoFiles.at(i) == "fedora.repo")
        {
            QString file;
            file = directory.path();
            file.append("/");
            file.append(repoFiles.at(i));
            QSettings *settings;
            settings = new QSettings(file , QSettings::NativeFormat);
            settings->beginGroup("fedora");
            settings->setValue("baseurl" ,_aIniFileF );
            settings->endGroup();

            delete settings;
            fixSharp(file);


        }

        else if(repoFiles.at(i) == "fedora-update.repo")
        {
            QString file;
            file = directory.path();
            file.append("/");
            file.append(repoFiles.at(i));
            QSettings *settings;
            settings = new QSettings (file , QSettings::NativeFormat);
            settings->beginGroup("updates");
            settings->setValue("baseurl" ,_aIniFileFU );
            settings->endGroup();

            delete  settings;
            fixSharp(file);

        }
    }


    //finish main Fedora and start RpmFusion
    QUrl adressRpmFusion{"https://mirrors.rpmfusion.org/mm/publiclist/"};
    downloadWebsitRpmFusion(adressRpmFusion);

}

void MirroFinder::createDynamicLink(QString dynamic , QString whatis )
{
    QString dynamicLink = dynamic;
    QString isFedoraOrRpmfusion=whatis;

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmDynamicLink.ini", QSettings::IniFormat);

    settings.beginGroup("dynamicLink");
    _dynamicLinkIniFile = settings.value("alghorithmDynamicLink").toString();
    settings.endGroup();

    _dynamicLinkIniFile.replace("$link" , dynamicLink);
    _dynamicLinkIniFile.replace("$basearch" , _baseArch);
    _dynamicLinkIniFile.replace("$releasever" ,_releaseever);

    downloadDynamicLink(_dynamicLinkIniFile, isFedoraOrRpmfusion);
}

void MirroFinder::downloadDynamicLink(QString url , QString whatis)
{
    QUrl dynamicUrl = url;
    QString isFedoraOrRpmfusion=whatis;

    qDebug() << __FUNCTION__ << dynamicUrl;

    _manager = new QNetworkAccessManager(this);

    if (isFedoraOrRpmfusion == "fedora")
    {
        connect(_manager , &QNetworkAccessManager::finished , this , &MirroFinder::downloadFinishedDynamicLinkFedora);
    }
    else if (isFedoraOrRpmfusion == "rpmFusion" )
    {
        connect(_manager , &QNetworkAccessManager::finished , this , &MirroFinder::downloadFinishedDynamicLinkRpmfusion);
    }

    dynamicUrl = url;
    _request.setUrl(dynamicUrl);
    _manager->get(_request);
}

void MirroFinder::downloadFinishedDynamicLinkFedora(QNetworkReply *downloadFinishedDynamicLinkFedora)
{
    qDebug() << __FUNCTION__;

    QString dataDynamicLink = downloadFinishedDynamicLinkFedora->readAll();
    QStringList _finishLinkDynamicFedora;

    regexDynamicFedora.indexIn(dataDynamicLink);
    _finishLinkDynamicFedora = regexDynamicFedora.capturedTexts();
    int pos = 0;
    while ((pos = regexDynamicFedora.indexIn(dataDynamicLink, pos)) != -1)
    {
        _finishLinkDynamicFedora << regexDynamicFedora.cap();
        pos += regexDynamicFedora.matchedLength();
    }
    _LinkDynamicFedora = _finishLinkDynamicFedora.takeFirst();

    readLinksFirstItemOfListFedoraFinished();
}

void MirroFinder::downloadFinishedDynamicLinkRpmfusion(QNetworkReply *downloadFinishedDynamicLinkRpmFusion)
{
    qDebug() << __FUNCTION__;

    QString dataDynamicLink = downloadFinishedDynamicLinkRpmFusion->readAll();
    QStringList _finishLinkDynamicRpm;
    regexDynamicRpmFuison.indexIn(dataDynamicLink);
    _finishLinkDynamicRpm = regexDynamicRpmFuison.capturedTexts();
    int pos = 0;
    while ((pos = regexDynamicRpmFuison.indexIn(_linksRpm, pos)) != -1)
    {
        _finishLinkDynamicRpm << regexDynamicRpmFuison.cap();
        pos += regexDynamicRpmFuison.matchedLength();
    }
    _LinkDynamicRpm = _finishLinkDynamicRpm.takeFirst();

    readLinksFirstItemOfListRpmFuisionFinished();
}


// start main rpmfusion
void MirroFinder::downloadWebsitRpmFusion(QUrl url)
{
    _manager = new QNetworkAccessManager(this);

    connect(_manager , &QNetworkAccessManager::finished , this , &MirroFinder::findLinksRpm);

    _URL = url;
    _request.setUrl(_URL);
    _manager->get(_request);

}

void MirroFinder::findLinksRpm(QNetworkReply *findLinks)
{

    data=findLinks->readAll();
    _linksRpm=data;

    int pos = 0;

    while ((pos = regexRpmFusion.indexIn(_linksRpm, pos)) != -1)
    {
        _listLinksRpm << regexRpmFusion.cap(1);
        pos += regexRpmFusion.matchedLength();
    }

    QStringList list = _listLinksRpm;
    list = list.toSet().toList();

    _listLinksRpm = list;

    readLinksFirstItemOfListRpm();
}

void MirroFinder::readLinksFirstItemOfListRpm()
{
    if(_queuRpm.isEmpty())
    {
        printFirstLinkRpmFusion();
    }

    else
    {
        _time.start();

        QString downloadFirstItem =  _queuRpm.takeFirst();
        createDynamicLink(downloadFirstItem , "rpmFusion");

    }
}

void MirroFinder::readLinksFirstItemOfListRpmFuisionFinished()
{
    QString finishLinkForItems = _dynamicLinkIniFile + _LinkDynamicRpm;
    _time.start();
    downloadItemsRpmFusion(finishLinkForItems);

}

void MirroFinder::downloadItemsRpmFusion(QUrl url)
{
    qDebug() << __FUNCTION__ << url;

    _manager = new QNetworkAccessManager(this);
    connect(_manager , &QNetworkAccessManager::finished , this , &MirroFinder::downloadItemsRpmFusionFinished);
    _request.setRawHeader("Range" , "bytes=" + QByteArray::number(0) + "-" + QByteArray::number(BYTE_DOWNLOAD_));
    _URL = url;
    _request.setUrl(_URL);
    _manager->get(_request);

}

void MirroFinder::downloadItemsRpmFusionFinished(QNetworkReply *reply)
{
    if(reply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    }

    _ntimeCurrent = _time.elapsed();
    QString finishLinkForItems  = _URL.toString();
    _calculateTimeRpm.insert(_ntimeCurrent , finishLinkForItems);

    qDebug() << __FUNCTION__ << _ntimeCurrent << finishLinkForItems;
    readLinksFirstItemOfListRpm();

}
void MirroFinder::printFirstLinkRpmFusion()
{
    qDebug() << __FUNCTION__;
    QString cutTheLinkRpm = _calculateTimeRpm.value(_ntimeCurrent);
    int pos2 =cutTheLinkRpm.lastIndexOf(QString("/releases"));
    _finalBestLinkRpm = cutTheLinkRpm.left(pos2);
    qDebug()  << _calculateTimeRpm.value(_ntimeCurrent);
    replaceToIniFileForRpm();
}

void MirroFinder::replaceToIniFileForRpm()
{
    qDebug() << __FUNCTION__;

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmRepo.ini", QSettings::IniFormat);

    //Alghoritm files rpmfusion-free.repo
    settings.beginGroup("rpmfusion-free");
    _aIniFileRF = settings.value("alghorithmRpmFusion").toString();
    settings.endGroup();

    _aIniFileRF.replace("$basearch" , _baseArch);
    _aIniFileRF.replace("$releasever" ,_releaseever);
    _aIniFileRF.replace("$link" , _finalBestLinkRpm);

    //Alghoritm files rpmfusion-free-updates.repo
    settings.beginGroup("rpmfusion-free-updates");
    _aIniFileRFU = settings.value("alghorithmRpmfusionFreeUpdate").toString();
    settings.endGroup();

    _aIniFileRFU.replace("$basearch" , _baseArch);
    _aIniFileRFU.replace("$releasever" ,_releaseever);
    _aIniFileRFU.replace("$link" , _finalBestLinkRpm);

    //Alghoritm files rpmfusion-nonfree.repo
    settings.beginGroup("rpmfusion-nonfree");
    _aIniFileRFUNonFree = settings.value("alghorithmRpmfusionNonFree").toString();
    settings.endGroup();

    _aIniFileRFUNonFree.replace("$basearch" , _baseArch);
    _aIniFileRFUNonFree.replace("$releasever" ,_releaseever);
    _aIniFileRFUNonFree.replace("$link" , _finalBestLinkRpm);

    //Alghoritm files rpmfusion-nonfree-updates.repo
    settings.beginGroup("rpmfusion-nonfree-updates");
    _aIniFileRFUNonFreeUpdate = settings.value("alghorithmRpmfusionNonFreeUpdate").toString();
    settings.endGroup();

    _aIniFileRFUNonFreeUpdate.replace("$basearch" , _baseArch);
    _aIniFileRFUNonFreeUpdate.replace("$releasever" ,_releaseever);
    _aIniFileRFUNonFreeUpdate.replace("$link" , _finalBestLinkRpm);

    writeToFileRpmFusion(ROOT_ETC_YUM_PATH);
}

void MirroFinder::writeToFileRpmFusion(QString addressFile)
{
    QDir directory(addressFile);
    QStringList repoFiles = directory.entryList(QStringList() << "*.repo" ,QDir::Files);

    for (int i = 0 ; i <= repoFiles.size(); i++)
    {

        if (repoFiles.at(i) == "rpmfusion-free.repo")
        {
            QString file;
            file = directory.path();
            file.append("/");
            file.append(repoFiles.at(i));
            QSettings * settings;
            settings  = new QSettings(file , QSettings::NativeFormat);
            settings->beginGroup("rpmfusion-free");
            settings->setValue("baseurl" ,_aIniFileRF );
            settings->endGroup();
            delete   settings;

            fixSharp(file);

        }

        else if (repoFiles.at(i) == "rpmfusion-free-updates.repo")
        {
            QString file;
            file = directory.path();
            file.append("/");
            file.append(repoFiles.at(i));
            QSettings *settings;
             settings = new QSettings(file , QSettings::NativeFormat);

            settings->beginGroup("rpmfusion-free-updates");
            settings->setValue("baseurl" ,_aIniFileRFU );
            settings->endGroup();
            delete settings;
            fixSharp(file);
        }

        else if (repoFiles.at(i) == "rpmfusion-nonfree.repo")
        {
            QString file;
            file = directory.path();
            file.append("/");
            file.append(repoFiles.at(i));
            QSettings * settings;
            settings = new QSettings(file , QSettings::NativeFormat);
            settings->beginGroup("rpmfusion-nonfree");
            settings->setValue("baseurl" ,_aIniFileRFUNonFree );
            settings->endGroup();
            delete  settings;
            fixSharp(file);
        }
        else if (repoFiles.at(i) == "rpmfusion-nonfree-updates.repo")
        {
            QString file;
            file = directory.path();
            file.append("/");
            file.append(repoFiles.at(i));
            QSettings * settings;
            settings = new  QSettings(file , QSettings::NativeFormat);
            settings->beginGroup("rpmfusion-nonfree-updates");
            settings->setValue("baseurl" ,_aIniFileRFUNonFreeUpdate );
            settings->endGroup();

            delete settings;
            fixSharp(file);
        }
    }

    qDebug () << "Finish";
}

void MirroFinder::fixSharp(const QString &path)
{
    QFile file(path);
    file.open(QFile::ReadOnly);

    QByteArray data = file.readAll();
    data.replace("#=" , "#metalink");
    data.replace("%23", "#");
    data.replace("metalink" , "#metalink");
    data.replace("#=" , "#metalink");

    file.close();
    file.open(QFile::WriteOnly);
    file.write(data);
    file.close();
}

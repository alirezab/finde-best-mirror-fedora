## Fedora Mirror Finder 

### A Simple Mirror Finder For Fedora
- Version 0.3

####  Dependencies used:
- Library Web Qt
- Library qt-base
- qt5-qtbase-devel


##### How to Generate a `MakeFile`

- `qmake-qt5` 



###### How to Compile:
<p>
You just need to make MirrorFinder executable using command bellow :

`make`
</p>

#### To run:

- Note that run using `sudo`
- <b>Only run once and enjoy speed. ;)</b>

<p>Dont Forget To Make It Executable</p>

- `sudo ./MirroFinder`

### Summary:


##### This Software Just Work for this Repositories :

<table style="width:100%">
  <tr>
    <th>Repository</th>
    <th>Repo detail</th>
  </tr>
  <tr>
    <td>Fedora</td>
    <td>Main Repository</td>
  </tr>
  <tr>
    <td>RpmFusion</td>
    <td>RpmFusion Free and RpmFuison Non-Free</td>
  </tr>
</table>

##################################################################
<p>
<b>contact me :</b>

`recive_mail_gitlab@riseup.net`
</p>
